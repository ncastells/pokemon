
import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';

import { Button, Form, FormGroup, Label, Input } from 'reactstrap';
import { Row, Col } from 'reactstrap';


class Edita extends Component {
    constructor(props) {
        super(props);

        //si no rebem paràmetre considerem item nou
        let id = 0;
        if (this.props.match && this.props.match.params.id) {
            id = this.props.match.params.id * 1;
        }

        this.state = new this.props.model(); //creem objecte buid del tipus del model
        this.state.tornar=false;
    
        this.carregaRegistre = this.carregaRegistre.bind(this);
        this.canviInput = this.canviInput.bind(this);
        this.submit = this.submit.bind(this);

        if (id>0){
            this.carregaRegistre(id);
        }
    }

    //carreguem registre a editar
    carregaRegistre(id) {
        this.props.model.getById(id)
            .then(data => this.setState(data))
            .catch(err => console.log(err));
    }

    //sincronitzem canvi a input
    canviInput(event) {
        const value = event.target.type === 'checkbox' ? event.target.checked : event.target.value;
        const name = event.target.name;
        this.setState({ [name]: value });
    }

    // submit
    submit(e) {
        //evitem comportament "normal" del formulari
        e.preventDefault();
        //creem objecte model amb les dades de state
        //el constructor només prendra les que corresponguin als fields
        let ob = new this.props.model(this.state);
        //desem i actualitzem state si ok
        ob.save()
            .then(() => this.setState({ volver: true }))
            .catch(err => {
                console.log(err);
                this.setState({ volver: true });
            });
    }


    render() {

        if (this.state.volver === true) {
            return <Redirect to={'/' + this.props.model.getCollection()} />
        }

        //creem tants inputs com camps del model i del tipus del model
        let inputs = this.props.model.getFields().map(item => (
            <Col xs="6" key={item.name} >
                <FormGroup >
                    <Label for={item.name + "Input"}>{item.name}</Label>
                    <Input type={item.type}
                        name={item.name}
                        id={item.name + "Input"}
                        value={this.state[item.name]}
                        onChange={this.canviInput} />
                </FormGroup>
            </Col>
        ));

        return (
            <>
                <h1 className="titol-vista">{"Edita " + this.props.model.getModelName()}</h1>
                <Form onSubmit={this.submit}>
                    <Row>
                        {inputs}
                    </Row>
                    <Row>
                        <Col>
                            <Button color="primary">Desar</Button>
                        </Col>
                    </Row>
                </Form>
            </>
        );
    }
}






export default Edita;
