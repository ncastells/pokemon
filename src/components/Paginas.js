import React from 'react';
import { Pagination, PaginationItem, PaginationLink } from 'reactstrap';

export default class Paginas extends React.Component {
  constructor(props){
    super(props);
      this.state={
        valorA: 1,
        pagina: 1
        // valorB: 2,
        // valorC: 3, 
        // valorD: 4, 
        // valorE: 5
      }
      this.suma=this.suma.bind(this);
      this.resta=this.resta.bind(this);
      this.pagina = this.pagina.bind(this);
    
  }

  pagina(){

  };

  resta(){
    this.setState({valorA: this.state.valorA-1})
  }

  suma(){
    this.setState({valorA: this.state.valorA+1})
  }

  render() {
    return (
      <Pagination aria-label="Page navigation example">
        <PaginationItem>
          <PaginationLink previous onClick={this.resta}/>
        </PaginationItem>
        <PaginationItem>
          <PaginationLink onClick={()=>this.props.onClick(this.state.valorA)} >
            {this.state.valorA}
          </PaginationLink>
        </PaginationItem>
        <PaginationItem>
          <PaginationLink onClick={()=>this.props.onClick(this.state.valorA+1)}>
          {this.state.valorA*1+1}
          </PaginationLink>
        </PaginationItem>
        <PaginationItem>
          <PaginationLink onClick={()=>this.props.onClick(this.state.valorA+2)}>
          {this.state.valorA*1+2}
          </PaginationLink>
        </PaginationItem>
        <PaginationItem>
          <PaginationLink onClick={()=>this.props.onClick(this.state.valorA+3)}>
          {this.state.valorA*1+3}
          </PaginationLink>
        </PaginationItem>
        <PaginationItem onClick={()=>this.props.onClick(this.state.valorA+4)}>
          <PaginationLink >
          {this.state.valorA*1+4}
          </PaginationLink>
        </PaginationItem>
        <PaginationItem>
          <PaginationLink next onClick={this.suma} />
        </PaginationItem>
      </Pagination>
    );
  }
}