import React from "react";

import { Button, Table } from 'reactstrap';
import { Redirect } from 'react-router-dom';
import Paginas from "./Paginas";

export default class Taula extends React.Component {


    constructor(props) {
        super(props);
        this.state = {
            data: [],
      
            itemDetalle: "",
            // pagina: 1
        }

        this.getData = this.getData.bind(this);
        this.detalle = this.detalle.bind(this);
        this.pagina = this.pagina.bind(this);
        this.getData(1);
    }



    //obté dades del model rebut via props
    getData(n = 1) {
        this.props.model.getAll(n)
            .then(data => this.setState({ data }))
            .catch(err => console.log(err));
    }


    detalle(nombre) {
        this.setState({ itemDetalle: nombre });
    }
    //cambiamos de página

    pagina(pagina) {
        // console.log(pagina);
        // this.setState({ pagina: pagina });
        this.getData(pagina);
    }


    render() {
        if (this.state.data.length === 0) {
            return <></>;
        }

        //foto detalle -------------------------------------------------
        if (this.state.itemDetalle) {
            return <Redirect to={`/${this.props.model.getCollection()}/detalle/${this.state.itemDetalle}`} />
        }


        let fieldNames = this.props.model.getFields();
        // titols de les columnes a partir dels camps del model
        let titolsColumnes = fieldNames.map(el => <th key={el.name}>{el.name}</th>);

        //creem les files a partir de dades rebudes, ordenades per id
        let filas = this.state.data.sort((a, b) => a.id - b.id).map(item => {
            let imagen = `https://img.pokemondb.net/sprites/omega-ruby-alpha-sapphire/dex/normal/${item.nombre.toLowerCase()}.png`;
            let cols = fieldNames.map(el => <td key={el.nombre}>{item[el.nombre]}</td>)
            return (
                <tr key={item.id}>
                    <td >{item.id}</td>
                    <td><img src={imagen}></img></td>
                    <td>{item.nombre}</td>
                    <td>{item.caracter}</td>
                    {cols}
                    <td><i onClick={() => this.detalle(item.nombre)} className="fa fa-edit clicable blue-icon" /></td>
                    {/* <td><i onClick={() => this.delete(item.id)} className="fa fa-trash clicable red-icon" /></td> */}
                </tr>
            );
        })


        return (
            <>
                <h1 className="titol-vista">{this.props.model.getTitle()}</h1>
                <Table>
                    <thead>
                        <tr>
                            <th>#</th>
                            <th></th>
                            {titolsColumnes}
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        {filas}
                    </tbody>
                </Table>
                <Paginas onClick={this.pagina} />


            </>
        );
    }


}

