import React from "react";
import { BrowserRouter, Link, NavLink, Switch, Route } from "react-router-dom";
import { Container, Row, Col, Button } from 'reactstrap';

// importem components
import Inici from './components/Inici';
import Taula from './components/Taula';
import NavMenu from './components/NavMenu';
// import Edita from './components/Edita';
import P404 from './components/P404';
import Detalle from './components/Detalle';
import Paginas from "./components/Paginas";

//importem models
import Pokemon from './models/Pokemon';

// importamos css
import 'bootstrap/dist/css/bootstrap.min.css';
import 'font-awesome/css/font-awesome.min.css';




//xmysql -h localhost -u root -d pokemon

// clase App 
export default class App extends React.Component {
  




  render() {
    return (
      <BrowserRouter>

      <NavMenu />
        <Container>

            <Row>
            <Col>
              <Switch>
                <Route exact path="/" component={Inici} />
                <Route exact path="/pokemons" render={() => <Taula model={Pokemon} />} />
                <Route exact path="/pokemons/detalle/:id" render={(props) => <Detalle {...props} model={Pokemon} />} />
                {/* <Route path="/usuaris/nou" render={() => <Edita model={Pokemon} />} />
                <Route path="/usuaris/edita/:id" render={(props) => <Edita {...props} model={Pokemon} />} />  */}
                <Route component={P404} />
              </Switch>
            </Col>
          </Row>

        </Container>
      </BrowserRouter>
    );

  }

}

